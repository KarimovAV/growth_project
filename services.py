import os
from enum import Enum


class Var(Enum):
    ROOT_DIR = os.path.dirname(os.path.abspath(__file__))