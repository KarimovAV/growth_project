import time

import pytest


class TestPlus:
    def test_positive(self):
        time.sleep(4)
        exp_value = 1 + 1
        assert exp_value == 2

    @pytest.mark.super_mega_test
    def test_minus(self):
        time.sleep(4)
        exp_value = -1 - (-1)
        assert exp_value == -2
