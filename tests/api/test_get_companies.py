import allure
from src.allure import allure_labels
from src.heplers.asserts import Asserting


@allure_labels.scenario("Получение информации о компаниях")
class TestCreatePetStore:
    @allure_labels.priority("High")
    @allure.tag("POSITIVE")
    def test_get_company(self, api_client):
        with allure.step("Проверить, что код статуса успешен"):
            get_company = api_client.get_company_by_id(company_id=1)
            Asserting.response_status_code(response_code=get_company.status_code, correct_code=200)
