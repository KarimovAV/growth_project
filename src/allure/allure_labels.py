import allure


def scenario(name: str) -> allure.label:
    return allure.label("scenario", name)


def priority(name: str) -> allure.label:
    return allure.label("priority", name)
