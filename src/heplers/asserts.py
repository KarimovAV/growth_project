from hamcrest import assert_that, empty, equal_to, is_


class Asserting:
    @staticmethod
    def fields_are_equal(list_for_check: list):
        list_diffs = []
        for diffs in list_for_check:
            for key, value in diffs.items():
                if key == "iterable_item_removed":
                    list_diffs.append(f"{key} {value}")
                if key == "iterable_item_added":
                    list_diffs.append(f"{key} {value}")
        assert_that(list_diffs, is_(empty()), f"Значения в поле не соответствует ожидаемому:\n{list_diffs}")

    @staticmethod
    def values_are_equal(list_for_check: list):
        list_diffs = []
        for diffs in list_for_check:
            for key, value in diffs.items():
                if key == "value_changed":
                    list_diffs.append(f"{key}:{value}")
        assert_that(list_diffs, is_(empty()), f"Значение в поле не соответствует ожидаемым: {list_diffs}")

    @staticmethod
    def response_status_code(response_code: int, correct_code: int):
        assert_that(
            response_code,
            equal_to(correct_code),
            f"Код статуса ответа должен быть {correct_code}." f"Вернулся {response_code}",
        )
