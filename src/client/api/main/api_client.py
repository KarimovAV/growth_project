from httpx import Response

from src.client.api.base_client import HTTPClient


class ApiClient(HTTPClient):
    """ "
    Описание эндпоинтов и url конкретного сервиса
    """

    def __init__(self, base_url: str = None):
        super().__init__(base_url=base_url)

    def get_companies(self) -> Response:
        return self.get(url="/api/companies")

    def get_company_by_id(self, company_id: int) -> Response:
        return self.get(url=f"/api/companies/{company_id}")
