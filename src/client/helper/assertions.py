import allure

from typing import Dict, List, Union


class Asserting:
    @staticmethod
    def check_status_code(response, expected_status_code: int):
        with allure.step(f"Проверяем, что {expected_status_code=}"):
            assert (
                response.status_code == expected_status_code
            ), (f"Полученный status_code={response.status_code} не равен ожидаемому {expected_status_code}: "
                f"{response.json()}")

    @staticmethod
    def content_is_empty(content: Union[Dict, List]):
        with allure.step("Проверяем, что контент ответа пустой"):
            assert not bool(content), "Получили непустой ответ"

    @staticmethod
    def content_is_not_empty(content: Union[Dict, List]):
        with allure.step("Проверяем, что контент ответа не пустой"):
            assert bool(content), "Получили пустой ответ"
