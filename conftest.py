import os

import pytest
from dotenv import load_dotenv

from services import Var
from src.client.api.main.api_client import ApiClient


@pytest.fixture(scope="function")
def api_client(configure_env) -> ApiClient:
    return ApiClient(base_url=os.environ.get("RESOURCE_URL"))


@pytest.fixture(scope="function")
def ssh_connect(configure_env):
    return os.environ.get("PKEY")


@pytest.fixture(scope="function")
def configure_env():
    dotenv_path = os.path.join(Var.ROOT_DIR.value, ".env")
    if os.path.exists(dotenv_path):
        return load_dotenv(dotenv_path)
